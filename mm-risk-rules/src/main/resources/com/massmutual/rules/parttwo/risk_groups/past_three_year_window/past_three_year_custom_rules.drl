package com.massmutual.rules.parttwo.risk_groups.past_three_year_window;

import com.massmutual.domain.PartTwoRiskRating;
import com.massmutual.domain.PartTwoRiskType;
import com.massmutual.domain.parttwo.PastThreeYearWindow;
import com.massmutual.domain.parttwo.MedicalCondition;
import java.util.ArrayList;
import java.lang.Integer;

dialect "mvel"

rule "PastThreeYearWindow - No Routine Exams and No Musculoskeletal Exams"
agenda-group "calculate-risk-rating"

when
	//Response of N or NS to hadEORoutine question AND response of N or NS to hadEOMusculoskeletal question
	PastThreeYearWindow ( ( conditionsNotFound contains  "hadEORoutine"         || conditionsNotSure contains "hadEORoutine" ) &&
						  ( conditionsNotFound contains  "hadEOMusculoskeletal" || conditionsNotSure contains "hadEOMusculoskeletal" ) )

then

PartTwoRiskRating $risk = new PartTwoRiskRating();
$risk.setRuleName( "P3YW_2_NoExamsRoutineOrMusculoskeletal" );
$risk.setDescription( "Were you examined for routine medical care? Were you examined for a minor minor musculoskeletal injury/pain? (response of no to both)" );
$risk.setPartTwoRiskType( PartTwoRiskType.PAST_THREE_YEAR_WINDOW );
$risk.setUPNT( 2 );
$risk.setSPNT( 2 );
$risk.setNONT( 2 );
$risk.setSPT( 2 );
$risk.setTOB( 2 );
$risk.setWP( 2 );
$risk.setDIIO( 2 );
insert( $risk );
end

rule "PastThreeYearWindow - No Routine Exams and Yes Musculoskeletal Exams"
agenda-group "calculate-risk-rating"

when
	//Response of N to hadEORoutine question AND response of Y to hadEOMusculoskeletal question
	PastThreeYearWindow ( conditionsNotFound contains  "hadEORoutine" &&
                                               conditionsFound contains  "hadEOMusculoskeletal")

then

PartTwoRiskRating $risk = new PartTwoRiskRating();
$risk.setRuleName( "P3YW_2_ExamsMusculoskeletal" );
$risk.setDescription( "Were you examined for routine medical care? (response of no) Were you examined for a minor minor musculoskeletal injury/pain? (response of yes)" );
$risk.setPartTwoRiskType( PartTwoRiskType.PAST_THREE_YEAR_WINDOW );
$risk.setUPNT( 1 );
$risk.setSPNT( 1 );
$risk.setNONT( 1 );
$risk.setSPT( 1 );
$risk.setTOB( 1 );
$risk.setWP( 1 );
$risk.setDIIO( 2 );
insert( $risk );
end