package com.massmutual.rules.parttwo.risk_groups.family_history;

import com.massmutual.domain.PartTwoRiskRating;
import com.massmutual.domain.PartTwoRiskType;
import com.massmutual.domain.parttwo.FamilyMember;
import com.massmutual.domain.parttwo.FamilyMemberType;
import com.massmutual.domain.parttwo.FamilyMemberDiseaseType;
import com.massmutual.domain.parttwo.FamilyMemberConditionType;
import com.massmutual.domain.parttwo.FamilyMemberLifeStatusType;
import com.massmutual.domain.parttwo.FamilyMemberInformationSource;

import java.util.ArrayList;


agenda-group "calculate-risk-rating"
dialect "mvel"

//If we have 2 or more mother + sister(s) or sister + sister(s)
//with breast cancer or ovarian cancer at any age - response from Family Conditions.

rule "Family History - 2 or more mother and daughter combo with breast cancer or ovarian cancer at any age"

when
$members : ArrayList(size > 1)
		from collect (FamilyMember(	
		member == FamilyMemberType.MOTHER.name() ||
		member == FamilyMemberType.SISTER.name(),
		diseaseType == FamilyMemberDiseaseType.BREAST_CANCER.name() || 
		diseaseType == FamilyMemberDiseaseType.OVARIAN_CANCER.name(),
		informationSource == FamilyMemberInformationSource.FAMILY_CONDITIONS.name()))
			
then
	
PartTwoRiskRating $risk = new PartTwoRiskRating();
$risk.setRuleName( "FH_1_FC_2WithBCorOC" );
$risk.setDescription( "Have any family members ever been diagnosed by a member of the medical profession with cancer, heart disease, diabetes or vascular disease? (response mother/daughter 2 or more combination with breast or ovarian)" );
$risk.setPartTwoRiskType( PartTwoRiskType.FAMILY_HISTORY );
$risk.setUPNT( 3 );
$risk.setSPNT( 3 );
$risk.setNONT( 2 );
$risk.setSPT( 3 );
$risk.setTOB( 2 );
$risk.setWP( 2 );
$risk.setDIIO( 2 );
insert( $risk );
end


//If we have 3 or more (any type of member) with colon cancer at any age, response from Family Conditions.

rule "Family History - 3 or more (any type of member) with colon cancer at any age"

when
$members : ArrayList(size > 2)
		from collect (FamilyMember(
		diseaseType == FamilyMemberDiseaseType.COLON_CANCER.name(),
		informationSource == FamilyMemberInformationSource.FAMILY_CONDITIONS.name()))
			
then

PartTwoRiskRating $risk = new PartTwoRiskRating();
$risk.setRuleName( "FH_1_FC_3WithCC" );
$risk.setDescription( "Have any family members ever been diagnosed by a member of the medical profession with cancer, heart disease, diabetes or vascular disease? (response 3 or more any member combination with colon cancer)" );
$risk.setPartTwoRiskType( PartTwoRiskType.FAMILY_HISTORY );
$risk.setUPNT( 3 );
$risk.setSPNT( 3 );
$risk.setNONT( 2 );
$risk.setSPT( 3 );
$risk.setTOB( 2 );
$risk.setWP( 2 );
$risk.setDIIO( 2 );
insert( $risk );
end

//If we have 2 or more sister + sister(s)
//with breast or ovarian cancer at any age, reponse from Sister Details

rule "Family History - 2 or more sisters with breast or ovarian cancer at any age"

when
$members : ArrayList(size > 1)
		from collect (FamilyMember(	
		member == FamilyMemberType.SISTER.name(),
		condition == FamilyMemberConditionType.BREAST_OR_OVARIAN_CANCER.name(),
		informationSource == FamilyMemberInformationSource.SISTER_DETAILS.name()))
			
then
	
PartTwoRiskRating $risk = new PartTwoRiskRating();
$risk.setRuleName( "FH_1_SD_2WithBCorOC" );
$risk.setDescription( "Your sister died of: (response 2 or more sisters with breast or ovarian cancer)" );
$risk.setPartTwoRiskType( PartTwoRiskType.FAMILY_HISTORY );
$risk.setUPNT( 3 );
$risk.setSPNT( 3 );
$risk.setNONT( 2 );
$risk.setSPT( 3 );
$risk.setTOB( 2 );
$risk.setWP( 2 );
$risk.setDIIO( 2 );
insert( $risk );
end

//If at least 1 sister has breast cancer and at least 1 sister has ovarian cancer, 
//at any age, reponse from Sister Details

rule "Family History - sisters with breast and ovarian cancer at any age"

when

//Have at least one sister with breast cancer
$bcMembers : ArrayList(size > 0)
		from collect (FamilyMember(	
		member == FamilyMemberType.SISTER.name(),
		condition == FamilyMemberConditionType.BREAST_CANCER.name(),
		informationSource == FamilyMemberInformationSource.SISTER_DETAILS.name()))

//AND have at least one sister with ovarian cancer		
$ocMembers : ArrayList(size > 0)
		from collect (FamilyMember(	
		member == FamilyMemberType.SISTER.name(),
		condition == FamilyMemberConditionType.OVARIAN_CANCER.name(),
		informationSource == FamilyMemberInformationSource.SISTER_DETAILS.name()))
	
then
	
PartTwoRiskRating $risk = new PartTwoRiskRating();
$risk.setRuleName( "FH_1_SD_2WithBCandOC" );
$risk.setDescription( "Your sister died of: (response at least 1 with breast and at least 1 with ovarian cancer)" );
$risk.setPartTwoRiskType( PartTwoRiskType.FAMILY_HISTORY );
$risk.setUPNT( 3 );
$risk.setSPNT( 3 );
$risk.setNONT( 2 );
$risk.setSPT( 3 );
$risk.setTOB( 2 );
$risk.setWP( 2 );
$risk.setDIIO( 2 );
insert( $risk );
end