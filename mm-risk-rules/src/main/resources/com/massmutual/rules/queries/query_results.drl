package com.massmutual.queries;

import com.massmutual.domain.PartOneRiskRating;
import com.massmutual.domain.PartTwoRiskRating;
import com.massmutual.domain.mvr.MVRRiskRating;
import com.massmutual.domain.mib.MIBRiskRating;
import com.massmutual.domain.RiskRating;
import com.massmutual.domain.RiskRatingRollup;
import com.massmutual.domain.RiskRatingSummary;
import com.massmutual.domain.ValidationMessage;

import com.massmutual.domain.verification.VerificationRiskRating;

import com.massmutual.domain.RiskSummaryType;

dialect "mvel"

// ######## RATINGS ###############

query getPartOneRiskRatings ( )
    $rating : RiskRating ( )
end;

query getPartTwoRiskRatings ( )
    $rating : PartTwoRiskRating ( )
end;

query getMVRRiskRatings ( )
    $rating : MVRRiskRating ( )
end;

query getMIBRiskRatings ( )
    $rating : MIBRiskRating ( )
end;

query getRxRiskRatings ( )
    $rating : RiskRating ( riskSummaryType == RiskSummaryType.RX_RULES)
end;

query getVerificationRatings ()
    $rating : VerificationRiskRating ()
end;


// ######## ROLLUPS ###############

query getPartOneRiskRatingRollup ( )
    $rollup : RiskRatingRollup ( riskSummaryType ==  RiskSummaryType.PART_I )
end;

query getPartTwoRiskRatingRollup ( )
    $rollup : RiskRatingRollup ( riskSummaryType ==  RiskSummaryType.PART_II )
end;

query getMVRRiskRatingRollup ( )
    $rollup : RiskRatingRollup (riskSummaryType ==  RiskSummaryType.MVR )
end;

query getMIBRiskRatingRollup ( )
    $rollup : RiskRatingRollup (riskSummaryType ==  RiskSummaryType.MIB )
end;

query getRxRiskRatingRollup ( )
    $rollup : RiskRatingRollup (riskSummaryType ==  RiskSummaryType.RX_RULES )
end;

query getVerificationRatingRollup ( )
    $rollup : RiskRatingRollup (riskSummaryType ==  RiskSummaryType.VERIFICATION )
end;

// ######## SUMMARY ###############

query getRiskRatingSummary ( )
    $summary : RiskRatingSummary ( )
end;

query getPartOneRiskRatingSummary ( )
    $summary : RiskRatingSummary ( riskSummaryType ==  RiskSummaryType.PART_I )
end;

query getPartTwoRiskRatingSummary ( )
    $summary : RiskRatingSummary ( riskSummaryType ==  RiskSummaryType.PART_II )
end;

query getMVRRiskRatingSummary ( )
    $summary : RiskRatingSummary ( riskSummaryType ==  RiskSummaryType.MVR )
end;

query getMIBRiskRatingSummary ( )
    $summary : RiskRatingSummary ( riskSummaryType ==  RiskSummaryType.MIB )
end;

query getRxRiskRatingSummary ( )
    $summary : RiskRatingSummary ( riskSummaryType ==  RiskSummaryType.RX_RULES )
end;

query getVerificationRollupSummary ( )
    $summary : RiskRatingSummary (riskSummaryType ==  RiskSummaryType.VERIFICATION )
end;

query getValidationMessages ( )
    $error : ValidationMessage ( )
end;